$(function () {
    $(document).on('click', '.toggle-process-order', function () {
        let order_id = $(this).data('order');

        Swal.fire({
            title: 'Process Order',
            text: 'Do you want to continue?',
            icon: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, continue'
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    url: '/order/process',
                    type: 'post',
                    contentType:"application/json; charset=utf-8",
                    data: JSON.stringify({ order_id: order_id }),
                    dataType: 'json',
                    success: function (response) {
                        if (response.success) {
                            $('#orders-container').html(response.data.html);

                            Swal.fire({
                                title: 'Processing Order',
                                icon: 'info'
                            });
                        } else {
                            Swal.fire({
                                title: 'Oops! Something went wrong.',
                                icon: 'error',
                                text: response.message
                            });
                        }
                    }
                });
            }
        });
    });

    $(document).on('click', '.toggle-edit-order', function () {
        let order_id = $(this).data('order');
        $('#toggle-order-row-' + order_id).removeClass('d-none');
    });

    $(document).on('click', '.toggle-order-update-row', function () {
        let order_id = $(this).data('order');
        let box_id = $('#box-id-' + order_id).val();

        $.ajax({
            url: '/order/assign-box-id',
            type: 'post',
            contentType:"application/json; charset=utf-8",
            data: JSON.stringify({
                order_id: order_id,
                box_id: box_id
            }),
            dataType: 'json',
            success: function (response) {
                if (response.success) {
                    $('#orders-container').html(response.data.html);

                    Swal.fire({
                        title: 'Order Updated!',
                        icon: 'success',
                        text: response.message
                    });
                } else {
                    Swal.fire({
                        title: 'Oops! Something went wrong.',
                        icon: 'error',
                        text: response.message
                    });
                }
            }
        });
    });

    $(document).on('click', '.toggle-order-cancel-row', function () {
        let order_id = $(this).data('order');
        $('#toggle-order-row-' + order_id).addClass('d-none');
    });

    $(document).on('click', '.toggle-order-detail', function () {
        let order_id = $(this).data('order');

        $.ajax({
            url: '/order/' + order_id,
            dataType: 'json',
            success: function (response) {
                if (response.success) {
                    $('#order-details-wrapper').html(response.data.html);
                    $('#order-details-wrapper').removeClass('d-none');
                    $('#order-list-wrapper').addClass('d-none');
                }
            }
        });
    });

    $(document).on('click', '#toggle-back', function () {
        $('#order-details-wrapper').addClass('d-none');
        $('#order-list-wrapper').removeClass('d-none');
    });

    $(document).on('click', '#toggle-report-back', function () {
        $('#report-order-wrapper').addClass('d-none');
        $('#order-list-wrapper').removeClass('d-none');
    });

    $(document).on('click', '.toggle-change-log', function () {
        let order_id = $(this).data('order');

        $.ajax({
            url: '/change/logs/' + order_id,
            dataType: 'json',
            success: function (response) {
                if (response.success) {
                    $('#logs-wrapper').html(response.data.html);
                    $('#logs-wrapper').removeClass('d-none');
                    $('#order-list-wrapper').addClass('d-none');
                }
            }
        });
    });

    $(document).on('click', '#toggle-log-back', function () {
        $('#logs-wrapper').addClass('d-none');
        $('#order-list-wrapper').removeClass('d-none');
    });

    $(document).on('click', '.toggle-report-order', function () {
        let order_id = $(this).data('order');

        $.ajax({
            url: '/order/report/' + order_id,
            dataType: 'json',
            success: function (response) {
                if (response.success) {
                    $('#report-order-wrapper').html(response.data.html);
                    $('#report-order-wrapper').removeClass('d-none');
                    $('#order-list-wrapper').addClass('d-none');
                }
            }
        });
    });

    $(document).on('click', '#toggle-submit-report', function () {
        let order_id = $(this).data('order');

        Swal.fire({
            title: 'Report Item Issue',
            icon: 'warning',
            text: "Continue to report this item issue?",
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, continue'
        }).then((response) => {
            $.ajax({
                url: '/order/report/' + order_id,
                type: 'post',
                contentType:"application/json; charset=utf-8",
                data: JSON.stringify({
                    status: $('#shipping-status').val(),
                    reason: $('#reason').val()
                }),
                dataType: 'json',
                success: function (response) {
                    if (response.success) {
                        $('#report-order-wrapper').addClass('d-none');
                        $('#orders-container').html(response.data.html);
                        $('#order-list-wrapper').removeClass('d-none');
                    } else {
                        Swal.fire({
                            title: 'Oops! Something went wrong.',
                            icon: 'error',
                            text: response.message
                        });
                    }
                },
                error: function (err) {
                    Swal.fire({
                        title: 'Oops! Something went wrong.',
                        icon: 'error',
                        text: err.responseJSON.message
                    });
                }
            });
        });
    });

    $(document).on('click', '.toggle-shipped-order', function () {
        let order_id = $(this).data('order');

        $.ajax({
            url: '/order/ship/' + order_id,
            dataType: 'json',
            success: function (response) {
                if (response.success) {
                    $('#ship-order-wrapper').html(response.data.html);
                    $('#ship-order-wrapper').removeClass('d-none');
                    $('#order-list-wrapper').addClass('d-none');
                }
            }
        });
    });

    $(document).on('click', '#toggle-submit-ship-order', function () {
        let order_id = $(this).data('order');

        Swal.fire({
            title: 'Ship Order',
            icon: 'question',
            text: "Continue to ship this order?",
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, continue'
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    url: '/order/ship/' + order_id,
                    type: 'post',
                    data: JSON.stringify({
                        shipping_company: $('#shipping-company').val(),
                        tracking_no: $('#tracking-no').val()
                    }),
                    dataType: 'json',
                    success: function (response) {
                        if (response.success) {
                            $('#orders-container').html(response.data.html);
                            $('#ship-order-wrapper').addClass('d-none');
                            $('#order-list-wrapper').removeClass('d-none');

                            Swal.fire({
                                title: 'Order shipped!',
                                icon: 'success'
                            });
                        } else {
                            Swal.fire({
                                title: 'Oops! Something went wrong.',
                                icon: 'error',
                                text: response.message
                            });
                        }
                    }
                });
            }
        });
    });

    $(document).on('click', '#toggle-filter', function () {
        $.ajax({
            url: '/order/filter',
            type: 'post',
            data: JSON.stringify({ status: $('#filter-order-status').val() }),
            dataType: 'json',
            success: function (response) {
                if (response.success) {
                    $('#orders-container').html(response.data.html);
                } else {
                    Swal.fire({
                        title: 'No order found',
                        icon: 'warning'
                    });
                }
            }
        });
    });

    $(document).on('click', '#toggle-search', function () {
        $.ajax({
            url: '/order/search',
            type: 'post',
            data: JSON.stringify({ order_id: $('#search-order-id').val() }),
            dataType: 'json',
            success: function (response) {
                if (response.success) {
                    $('#orders-container').html(response.data.html);
                } else {
                    Swal.fire({
                        title: 'No order found',
                        icon: 'warning'
                    });
                }
            }
        });
    });
});