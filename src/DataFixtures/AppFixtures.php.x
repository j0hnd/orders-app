<?php

namespace App\DataFixtures;

use App\Entity\Department;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Faker\Factory;
use DateTime;

class AppFixtures extends Fixture
{
	private $userPasswordHasher;


	public function __construct(UserPasswordHasherInterface $userPasswordHasher)
	{
		$this->userPasswordHasher = $userPasswordHasher;
	}

    public function load(ObjectManager $manager)
    {
		$faker = Factory::create();

		$departments = ['Picking Department', 'Shipping Department', 'Management Department'];

		foreach ($departments as $department_name) {
			// create department
			$department = new Department();

			$department
				->setName($department_name);
//				->setCreatedAt(new DeteTime('now'));

			$manager->persist($department);

			// create user
			$user = new User();
			$user
				->setName($faker->name)
				->setEmail($faker->safeEmail)
				->setPassword($this->userPasswordHasher->hashPassword($user, 'test'))
				->setDepartmentId($department->getId());
//				->setCreatedAt(new DateTime('now'));

			$manager->persist($user);
		}

        $manager->flush();
    }
}
