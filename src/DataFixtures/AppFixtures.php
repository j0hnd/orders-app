<?php

namespace App\DataFixtures;

use App\Entity\Department;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;


class AppFixtures extends Fixture
{
	public function load(ObjectManager $manager)
	{
		$faker = Factory::create();

		$departments = ['Picking Department', 'Shipping Department', 'Management Department'];

		foreach ($departments as $department_name) {
			// create department
			$department = new Department();

			$department
				->setName($department_name)
				->setCreatedAt($faker->dateTime('now'));

			$manager->persist($department);
			$manager->flush();

			// create user
			$user = new User();
			$user
				->setName($faker->name)
				->setEmail($faker->safeEmail)
				->setPassword(password_hash('test', PASSWORD_DEFAULT))
				->setDepartmentId($department->getId())
				->setCreatedAt($faker->dateTime('now'));

			$manager->persist($user);
			$manager->flush();
		}
	}
}
