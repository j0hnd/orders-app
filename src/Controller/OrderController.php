<?php

namespace App\Controller;

use App\Entity\Order;
use App\Entity\ShippingDetail;
use App\Repository\ChangeLogRepository;
use App\Repository\OrderRepository;
use App\Repository\ShippingDetailRepository;
use Doctrine\DBAL\Driver\Exception;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RequestStack;


class OrderController extends AbstractController
{
    private $orderRepository;

    private $shippingDetailRepository;

    private $changeLogRepository;

    private $validator;

    private $requestStack;


    public function __construct(
        OrderRepository $orderRepository,
        ShippingDetailRepository $shippingDetailRepository,
        ChangeLogRepository $changeLogRepository,
        RequestStack $requestStack,
        ValidatorInterface $validator
    )
    {
        $this->orderRepository = $orderRepository;
        $this->shippingDetailRepository = $shippingDetailRepository;
        $this->changeLogRepository = $changeLogRepository;
        $this->requestStack = $requestStack;
        $this->validator = $validator;
    }

    /**
     * @Route("/orders/{department}", name="order", defaults={"department"="picking"})
     */
    public function index($department): Response
    {
        switch ($department) {
            case "shipping":
                $orders = $this->orderRepository->getReadyToShipOrders();
                break;

            case "management":
                $orders = $this->orderRepository->getOrders();
                break;

            default:
                $orders = $this->orderRepository->getReceivedOrders();
                $department = 'picking';
                break;
        }

        $this->requestStack->getSession()->set('department', $department);


        return $this->render('order/index.html.twig', [
            'controller_name' => 'OrderController',
            'orders' => $orders,
            'department' => $department
        ]);
    }

    /**
     * @param Request $request
     * @param String $order_id
     * @return JsonResponse
     * @Route("/order/{order_id<\d+>}", name="detail", methods={"GET"})
     */
    public function detail(Request $request, $order_id): JsonResponse
    {
        $success = false;
        $data = null;
        $message = "";
        $response_code = Response::HTTP_BAD_REQUEST;

        try {
            $rules = new Assert\Collection([
                'order_id' => [
                    new Assert\NotBlank(),
                    new Assert\Positive()
                ]
            ]);

            $validation_result = $this->validator->validate(['order_id' => $order_id], $rules);

            if (0 === count($validation_result)) {
                $order = $this->orderRepository->getOrderDetails($order_id);

                if ($order) {
                    $contents = $this->renderView('order/partials/details.html.twig', [
                        'order' => $this->orderRepository->getOrderDetails($order_id)
                    ]);

                    $success = true;
                    $data = ['html' => $contents];
                    $response_code = Response::HTTP_OK;
                }
            }
        } catch (\Exception $exception) {
            $message = $exception->getMessage();
            $response_code = Response::HTTP_BAD_GATEWAY;
        }


        return new JsonResponse([
            'success' => $success,
            'message' => $message,
            'data' => $data
        ], $response_code);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws Exception
     * @Route("/order/assign-box-id", name="assignOrderBoxId", methods={"POST"})
     */
    public function assignOrderBoxId(Request $request): JsonResponse
    {
        $success = false;
        $data = null;
        $message = "";
        $response_code = Response::HTTP_BAD_REQUEST;

        try {
            $data = json_decode($request->getContent(), true);

            $rules = new Assert\Collection([
                'order_id' => [
                    new Assert\NotBlank(),
                    new Assert\Positive()
                ],
                'box_id' => [
                    new Assert\NotBlank(),
                    new Assert\Positive()
                ]
            ]);

            $validation_result = $this->validator->validate($data, $rules);

            if (0 === count($validation_result)) {
                $order = $this->orderRepository->findOneBy(['id' => $data['order_id']]);

                if ($order) {
                    $order
                        ->setStatus(Order::ORDER_READY_TO_SHIP)
                        ->setBoxId($data['box_id'])
                        ->setUpdatedAt(new \DateTime('now'));

                    if ($this->orderRepository->update($order)) {
                        $contents = $this->renderView('order/partials/list.html.twig', [
                            'orders' => $this->orderRepository->getReceivedOrders(),
                            'department' => $this->requestStack->getSession()->get('department')
                        ]);

                        $this->changeLogRepository->createLog(['order_id' => $order->getId(), 'action' => Order::ORDER_READY_TO_SHIP]);

                        $success = true;
                        $message = "Order is ready to ship!";
                        $data = ['html' => $contents];
                        $response_code = Response::HTTP_OK;
                    }
                }
            } else {
                $response_code = Response::HTTP_NOT_FOUND;
            }
        } catch (\Exception $exception) {
            $message = $exception->getMessage();
            $response_code = Response::HTTP_BAD_GATEWAY;
        }


        return new JsonResponse([
            'success' => $success,
            'message' => $message,
            'data' => $data
        ], $response_code);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws Exception
     * @Route("/order/process", name="processOrder", methods={"POST"})
     */
    public function processOrder(Request $request): JsonResponse
    {
        $success = false;
        $data = null;
        $message = "";
        $response_code = Response::HTTP_BAD_REQUEST;

        try {
            $data = json_decode($request->getContent(), true);

            $rules = new Assert\Collection([
                'order_id' => [
                    new Assert\NotBlank(),
                    new Assert\Positive()
                ]
            ]);

            $validation_result = $this->validator->validate($data, $rules);

            if (0 === count($validation_result)) {
                $order = $this->orderRepository->findOneBy(['id' => $data['order_id']]);

                if ($order) {
                    $order->setStatus(Order::ORDER_PROCESSING);
                    $order->setUpdatedAt(new \DateTime('now'));

                    if ($this->orderRepository->update($order)) {
                        $this->changeLogRepository->createLog(['order_id' => $order->getId(), 'action' => Order::ORDER_PROCESSING]);

                        $contents = $this->renderView('order/partials/list.html.twig', [
                            'orders' => $this->orderRepository->getReceivedOrders(),
                            'department' => $this->requestStack->getSession()->get('department')
                        ]);

                        $success = true;
                        $message = "Processing order...";
                        $data = ['html' => $contents];
                        $response_code = Response::HTTP_OK;
                    }
                } else {
                    $response_code = Response::HTTP_NOT_FOUND;
                }
            }
        } catch (\Exception $exception) {
            $message = $exception->getMessage();
            $response_code = Response::HTTP_BAD_GATEWAY;
        }


        return new JsonResponse([
            'success' => $success,
            'message' => $message,
            'data' => $data
        ], $response_code);
    }

    /**
     * @param Request $request
     * @param String $order_id
     * @return JsonResponse
     * @Route("/order/report/{order_id<\d+>}", name="report", methods={"GET"})
     */
    public function report(Request $request, $order_id): JsonResponse
    {
        $success = false;
        $data = null;
        $message = "";
        $response_code = Response::HTTP_BAD_REQUEST;

        try {
            $rules = new Assert\Collection([
                'order_id' => [
                    new Assert\NotBlank(),
                    new Assert\Positive()
                ]
            ]);

            $validation_result = $this->validator->validate(['order_id' => $order_id], $rules);

            if (0 === count($validation_result)) {
                $order = $this->orderRepository->getOrderDetails($order_id);

                if ($order) {
                    $contents = $this->renderView('order/partials/report.html.twig', [
                        'order' => $this->orderRepository->getOrderDetails($order_id)
                    ]);

                    $success = true;
                    $data = ['html' => $contents];
                    $response_code = Response::HTTP_OK;
                }
            }
        } catch (\Exception $exception) {
            $message = $exception->getMessage();
            $response_code = Response::HTTP_BAD_GATEWAY;
        }


        return new JsonResponse([
            'success' => $success,
            'message' => $message,
            'data' => $data
        ], $response_code);
    }

    /**
     * @param Request $request
     * @param String $order_id
     * @return JsonResponse
     * @Route("/order/report/{order_id<\d+>}", name="submitReport", methods={"POST"})
     */
    public function submitReport(Request $request, $order_id): JsonResponse
    {
        $success = false;
        $data = null;
        $message = "";
        $response_code = Response::HTTP_BAD_REQUEST;

        try {
            $rules = new Assert\Collection([
                'order_id' => [
                    new Assert\NotBlank(),
                    new Assert\Positive()
                ],
                'status' => new Assert\NotBlank(),
                'reason' => new Assert\NotBlank()
            ]);

            $data = json_decode($request->getContent(), true);
            $data['order_id'] = $order_id;

            $validation_result = $this->validator->validate($data, $rules);

            if (0 === count($validation_result)) {
                $order = $this->orderRepository->getOrderDetails($order_id);

                if ($order) {
                    $shipping = $this->shippingDetailRepository->findOneBy(['id' => $order['shippingId']]);

                    if ($shipping) {
                        $shipping
                            ->setStatus($data['status'])
                            ->setRemarks($data['reason'])
                            ->setUpdatedAt(new \DateTime('now'));

                        if ($this->shippingDetailRepository->update($shipping)) {
                            $order_detail = $this->orderRepository->findOneBy(['id' => $data['order_id']]);

                            $order_detail
                                ->setStatus(Order::ORDER_PROCESSING)
                                ->setUpdatedAt(new \DateTime('now'));

                            $this->orderRepository->update($order_detail);

                            $this->changeLogRepository->createLog([
                                'order_id' => $order_detail->getId(),
                                'action' => Order::ORDER_PROCESSING,
                                'reason_status' => $data['status'],
                                'reason_details' => $data['reason']
                            ]);

                            $contents = $this->renderView('order/partials/list.html.twig', [
                                'orders' => $this->orderRepository->getReadyToShipOrders(),
                                'department' => $this->requestStack->getSession()->get('department')
                            ]);

                            $success = true;
                            $data = ['html' => $contents];
                            $response_code = Response::HTTP_OK;
                        }
                    }
                }
            } else {
                $message = "Please fill out the form accordingly";
            }
        } catch (\Exception $exception) {
            $message = $exception->getMessage();
            $response_code = Response::HTTP_BAD_GATEWAY;
        }


        return new JsonResponse([
            'success' => $success,
            'message' => $message,
            'data' => $data
        ], $response_code);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @Route("/order/ship/{order_id<\d+>}", name="shipOrder", methods={"GET"})
     */
    public function shipOrder(Request $request): JsonResponse
    {
        $success = false;
        $data = null;
        $message = "";
        $response_code = Response::HTTP_BAD_REQUEST;

        try {
            $rules = new Assert\Collection([
                'order_id' => [
                    new Assert\NotBlank(),
                    new Assert\Positive()
                ]
            ]);

            $order_id = $request->get('order_id');

            $validation_result = $this->validator->validate(['order_id' => $order_id], $rules);

            if (0 === count($validation_result)) {
                $contents = $this->renderView('order/partials/ship.html.twig', [
                    'order' => $this->orderRepository->getOrderDetails($order_id)
                ]);

                $success = true;
                $data = ['html' => $contents];
                $response_code = Response::HTTP_OK;
            } else {
                $message = "Missing Order ID";
            }
        } catch (\Exception $exception) {
            $message = $exception->getMessage();
            $response_code = Response::HTTP_BAD_GATEWAY;
        }


        return new JsonResponse([
            'success' => $success,
            'message' => $message,
            'data' => $data
        ], $response_code);
    }

    /**
     * @param Request $request
     * @param String $order_id
     * @return JsonResponse
     * @Route("/order/ship/{order_id<\d+>}", name="submitShipOrder", methods={"POST"})
     */
    public function submitShipOrder(Request $request, $order_id): JsonResponse
    {
        $success = false;
        $data = null;
        $message = "";
        $response_code = Response::HTTP_BAD_REQUEST;

        try {
            $rules = new Assert\Collection([
                'order_id' => [
                    new Assert\NotBlank(),
                    new Assert\Positive()
                ],
                'shipping_company' => new Assert\NotBlank(),
                'tracking_no' => new Assert\NotBlank(),
            ]);

            $data = json_decode($request->getContent(), true);
            $data['order_id'] = $order_id;

            $validation_result = $this->validator->validate($data, $rules);

            if (0 === count($validation_result)) {
                if ($order = $this->orderRepository->findOneBy(['id' => $order_id])) {
                    if ($shipping = $this->shippingDetailRepository->findOneBy(['id' => $order->getShippingId()])) {
                        $shipping
                            ->setShippingCompany($data['shipping_company'])
                            ->setTrackingNo($data['tracking_no'])
                            ->setStatus('shipped')
                            ->setUpdatedAt(new \DateTime('now'));

                        if ($this->shippingDetailRepository->update($shipping)) {
                            $order->setStatus(Order::ORDER_SHIPPED);
                            $this->orderRepository->update($order);

                            $this->changeLogRepository->createLog(['order_id' => $order->getId(), 'action' => Order::ORDER_SHIPPED]);

                            $contents = $this->renderView('order/partials/list.html.twig', [
                                'orders' => $this->orderRepository->getReadyToShipOrders(),
                                'department' => $this->requestStack->getSession()->get('department')
                            ]);

                            $success = true;
                            $data = ['html' => $contents];
                            $response_code = Response::HTTP_OK;
                        }
                    } else {
                        $message = "Shipping reference not found";
                    }
                } else {
                    $message = "Invalid Order Id";
                }
            }
        } catch (\Exception $exception) {
            $message = $exception->getMessage();
            $response_code = Response::HTTP_BAD_GATEWAY;
        }


        return new JsonResponse([
            'success' => $success,
            'message' => $message,
            'data' => $data
        ], $response_code);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @Route("/order/filter", name="filter", methods={"POST"})
     */
    public function filter(Request $request)
    {
        $success = false;
        $data = null;
        $message = "";
        $response_code = Response::HTTP_BAD_REQUEST;

        try {
            $rules = new Assert\Collection([
                'status' => new Assert\NotBlank()
            ]);

            $data = json_decode($request->getContent(), true);

            $validation_result = $this->validator->validate($data, $rules);

            if (0 === count($validation_result)) {
                $contents = $this->renderView('order/partials/list.html.twig', [
                    'orders' => $this->orderRepository->getOrders($data['status']),
                    'department' => 'management'
                ]);

                $success = true;
                $data = ['html' => $contents];
                $response_code = Response::HTTP_OK;
            }
        } catch (Exception $exception) {
            $message = $exception->getMessage();
            $response_code = Response::HTTP_BAD_GATEWAY;
        }


        return new JsonResponse([
            'success' => $success,
            'message' => $message,
            'data' => $data
        ], $response_code);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @Route("/order/search", name="serach", methods={"POST"})
     */
    public function search(Request $request)
    {
        $success = false;
        $data = null;
        $message = "";
        $response_code = Response::HTTP_BAD_REQUEST;

        try {
            $rules = new Assert\Collection([
                'order_id' => [
                    new Assert\NotBlank(),
                    new Assert\Positive()
                ]
            ]);

            $data = json_decode($request->getContent(), true);

            $validation_result = $this->validator->validate($data, $rules);

            if (0 === count($validation_result)) {
                $contents = $this->renderView('order/partials/list.html.twig', [
                    'orders' => $this->orderRepository->getOrders('all', $data['order_id']),
                    'department' => 'management'
                ]);

                $success = true;
                $data = ['html' => $contents];
                $response_code = Response::HTTP_OK;
            }
        } catch (Exception $exception) {
            $message = $exception->getMessage();
            $response_code = Response::HTTP_BAD_GATEWAY;
        }


        return new JsonResponse([
            'success' => $success,
            'message' => $message,
            'data' => $data
        ], $response_code);
    }
}