<?php

namespace App\Controller;

use App\Repository\ChangeLogRepository;
use phpDocumentor\Reflection\Types\Integer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ChangeLogController extends AbstractController
{
    private $changeLogRepository;

    public function __construct(ChangeLogRepository $changeLogRepository)
    {
        $this->changeLogRepository = $changeLogRepository;
    }

    /**
     * @param Request $request
     * @param Integer $order_id
     * @return JsonResponse
     * @Route("/change/logs/{order_id<\d+>}", name="logs", methods={"GET"})
     */
    public function logs(Request $request, $order_id): JsonResponse
    {
        $success = false;
        $data = null;
        $message = "";
        $response_code = Response::HTTP_BAD_REQUEST;

        $contents = $this->renderView('change_log/partials/log.html.twig', [
            'logs' => $this->changeLogRepository->findBy(['order_id' => $order_id])
        ]);

        if ($contents) {
            $success = true;
            $data = ['html' => $contents];
            $response_code = Response::HTTP_OK;
        }


        return new JsonResponse([
            'success' => $success,
            'message' => $message,
            'data' => $data
        ], $response_code);
    }
}
