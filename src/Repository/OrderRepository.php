<?php

namespace App\Repository;

use App\Entity\Order;
use App\Entity\OrderItem;
use App\Entity\ShippingDetail;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use phpDocumentor\Reflection\Types\Integer;

/**
 * @method Order|null find($id, $lockMode = null, $lockVersion = null)
 * @method Order|null findOneBy(array $criteria, array $orderBy = null)
 * @method Order[]    findAll()
 * @method Order[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrderRepository extends ServiceEntityRepository
{
    private $manager;


    public function __construct(ManagerRegistry $registry, EntityManagerInterface $manager)
    {
        parent::__construct($registry, Order::class);
        $this->manager = $manager;
    }

    /**
     * @return Order[] Returns an array of Order objects
     *
     */
    public function getReceivedOrders()
    {
        return $this->createQueryBuilder('orders')
            ->select('orders.id, orders.totalAmount, orders.discountAmount, orders.status, orders.createdAt, orders.deletedAt, orders.cancelledAt, order_item.itemName, orders.boxId')
            ->leftJoin(OrderItem::class, 'order_item', 'WITH', 'order_item.orderId = orders.id')
            ->where('orders.status in (:received, :processing)')
            ->andWhere('orders.deletedAt is null')
            ->andWhere('orders.cancelledAt is null')
            ->orderBy('orders.createdAt', 'asc')
            ->setParameter('received', Order::ORDER_RECEIVED)
            ->setParameter('processing', Order::ORDER_PROCESSING)
            ->getQuery()
            ->getResult();
    }

    /**
     * @return Order[] Returns an array of Order objects
     *
     */
    public function getReadyToShipOrders()
    {
        return $this->createQueryBuilder('orders')
            ->select('orders.id, orders.totalAmount, orders.discountAmount, orders.status, orders.createdAt, orders.deletedAt, orders.cancelledAt, order_item.itemName, orders.boxId')
            ->leftJoin(OrderItem::class, 'order_item', 'WITH', 'order_item.orderId = orders.id')
            ->where('orders.status = :status')
            ->andWhere('orders.deletedAt is null')
            ->andWhere('orders.cancelledAt is null')
            ->orderBy('orders.createdAt', 'asc')
            ->setParameter('status', Order::ORDER_READY_TO_SHIP)
            ->getQuery()
            ->getResult();
    }

    /**
     * @return Order[] Returns an array of Order objects
     *
     */
    public function getOrders($status = 'all', $order_id = null)
    {
        $q = $this->createQueryBuilder('orders')
            ->select('orders.id, orders.totalAmount, orders.discountAmount, orders.status, orders.createdAt, orders.deletedAt, orders.cancelledAt, order_item.itemName, orders.boxId')
            ->leftJoin(OrderItem::class, 'order_item', 'WITH', 'order_item.orderId = orders.id')
            ->where('orders.deletedAt is null')
            ->andWhere('orders.cancelledAt is null')
            ->orderBy('orders.createdAt', 'asc');

        if ($status != 'all') {
            $q->andWhere('orders.status = :status');
            $q->setParameter('status', $status);
        }

        if (! is_null($order_id)) {
            $q->andWhere('orders.id = :order_id');
            $q->setParameter('order_id', $order_id);
        }

        return $q->getQuery()->getResult();
    }

    /**
     * @param Integer $order_id
     * @return Order[] Returns an array of Order objects
     *
     */
    public function getOrderDetails($order_id)
    {
        return $this->createQueryBuilder('orders')
            ->select('orders.id, orders.totalAmount, orders.discountAmount, orders.status, orders.createdAt, orders.deletedAt, orders.cancelledAt, order_item.itemName, 
                            shipping.shippingCompany, shipping.trackingNo, orders.boxId, orders.shippingId, shipping.status, shipping.remarks')
            ->leftJoin(OrderItem::class, 'order_item', 'WITH', 'order_item.orderId = orders.id')
            ->leftJoin(ShippingDetail::class, 'shipping', 'WITH', 'shipping.id = orders.shippingId')
            ->where('orders.id = :order_id')
            ->andWhere('orders.deletedAt is null')
            ->andWhere('orders.cancelledAt is null')
            ->setParameter('order_id', $order_id)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function update(Order $order): Order
    {
        $this->manager->persist($order);
        $this->manager->flush();

        return $order;
    }


    // /**
    //  * @return Order[] Returns an array of Order objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Order
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
