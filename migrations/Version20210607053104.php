<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210607053104 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE departments (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(45) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE orders (id INT AUTO_INCREMENT NOT NULL, box_id INT DEFAULT NULL, shipping_id INT DEFAULT NULL, status VARCHAR(20) DEFAULT NULL, total_amount DOUBLE PRECISION DEFAULT NULL, discount_amount DOUBLE PRECISION DEFAULT NULL, added_by INT NOT NULL, cancelled_by INT DEFAULT NULL, cancelled_at DATETIME DEFAULT NULL, deleted_at DATETIME DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE order_items (id INT AUTO_INCREMENT NOT NULL, order_id INT NOT NULL, item_name VARCHAR(45) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE shipping_details (id INT AUTO_INCREMENT NOT NULL, shipping_company VARCHAR(45) NOT NULL, tracking_no VARCHAR(50) DEFAULT NULL, image_filename VARCHAR(100) DEFAULT NULL, status VARCHAR(20) NOT NULL, remarks LONGTEXT DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE users (id INT AUTO_INCREMENT NOT NULL, department_id INT NOT NULL, name VARCHAR(45) NOT NULL, email VARCHAR(100) NOT NULL, password VARCHAR(100) NOT NULL, deleted_at DATETIME DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');

        $this->addSql('CREATE INDEX shipping_id_idx ON orders (shipping_id)');
        $this->addSql('CREATE INDEX status_idx ON orders (status)');
        $this->addSql('CREATE INDEX added_by_idx ON orders (added_by)');
        $this->addSql('CREATE INDEX cancelled_by_idx ON orders (cancelled_by)');
        $this->addSql('CREATE INDEX order_id_idx ON order_items (order_id)');
        $this->addSql('CREATE INDEX tracking_no_idx ON shipping_details (tracking_no)');
        $this->addSql('CREATE INDEX shipping_status_idx ON shipping_details (status)');
        $this->addSql('CREATE INDEX department_id_idx ON users (department_id)');
        $this->addSql('CREATE INDEX email_idx ON users (email)');
        $this->addSql('CREATE INDEX department_name_idx ON departments (name)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE departments');
        $this->addSql('DROP TABLE orderss');
        $this->addSql('DROP TABLE order_items');
        $this->addSql('DROP TABLE shipping_details');
        $this->addSql('DROP TABLE users');
    }
}
